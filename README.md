# search-prefab

#### 介绍
快速查找包含指定文字的Unity预制体

#### 安装教程

```
npm i search-prefab
```

#### 使用说明

1.  查找指定文件夹下所有预制体

```
search-prefab -s G:/unityproj/Assets/AssetSources/ui -k 免费体验,查找关键字
```

2.  查找指定的预制体

```
search-prefab -s G:/unityproj/Assets/AssetSources/ui/GameView.prefab -k 免费体验,查找关键字
```

3.  查找当前目录下的所有预制体

```
search-prefab -k 免费体验,查找关键字
```

#### 示例

```
> search-prefab -s G:/ycxsweb/trunk/Assets/AssetSources/ui -k 免费体验

processing: G:\ycxsweb\trunk\Assets\AssetSources\ui\VideoPlayer.prefab


======================RESULT======================
G:\ycxsweb\trunk\Assets\AssetSources\ui\system\ShowGoldVipFunctionView.prefab --
  Text: 免费体验
```