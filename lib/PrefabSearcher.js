"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.PrefabSearcher = void 0;
var fs_1 = __importDefault(require("fs"));
var path_1 = __importDefault(require("path"));
var PrefabSearcher = /** @class */ (function () {
    function PrefabSearcher() {
        this.SectionPattern = /^--- !u!(\d+) &(\d+)$/;
        this.GameObjectPattern = /(?<=\s)m_GameObject: \{fileID: (\d+)\}/;
        this.NamePattern = /(?<=\s)m_Name: ([\s\S]*)/;
        this.TextPattern = /(?<=\s)(value|m_Text): (["']{1})([\s\S]*)/;
    }
    PrefabSearcher.prototype.startFind = function (srcPath, keywords) {
        this.result = [];
        this.findKeywordsInDir(srcPath, keywords);
        console.log('\n');
        console.log('======================RESULT======================');
        for (var _i = 0, _a = this.result; _i < _a.length; _i++) {
            var m = _a[_i];
            console.log(m.filePath + " -- ");
            console.log("  " + (m.goName || '') + ": " + m.matched);
        }
    };
    PrefabSearcher.prototype.findKeywordsInDir = function (srcPath, keywords) {
        var pathStat = fs_1.default.statSync(srcPath);
        if (pathStat.isDirectory()) {
            var files = fs_1.default.readdirSync(srcPath);
            for (var _i = 0, files_1 = files; _i < files_1.length; _i++) {
                var f = files_1[_i];
                var filePath = path_1.default.join(srcPath, f);
                var fileStat = fs_1.default.statSync(filePath);
                if (fileStat.isDirectory()) {
                    this.findKeywordsInDir(filePath, keywords);
                }
                else {
                    this.findKeywordsInFile(filePath, keywords);
                }
            }
        }
        else {
            this.findKeywordsInFile(srcPath, keywords);
        }
    };
    PrefabSearcher.prototype.findKeywordsInFile = function (filePath, keywords) {
        if (path_1.default.extname(filePath) == '.prefab') {
            var matched = this.findKeywordsInPrefab(filePath, keywords);
            if (matched)
                this.result.push(matched);
        }
    };
    PrefabSearcher.prototype.findKeywordsInPrefab = function (filePath, keywords) {
        console.log('\x1B[1A\x1B[Kprocessing: %s', filePath);
        var fileContent = fs_1.default.readFileSync(filePath, 'utf-8');
        var lines = fileContent.split(/\r?\n/);
        // 假如是该节点是预制体实例，则是，如斗罗韩服手游WorldUIElementView.prefab
        // - target: {fileID: 114479196432416642, guid: 5d66a490e5f6da842a0990a7a99f6bf1,
        //     type: 3}
        //   propertyPath: m_Text
        //   value: "\u51A5\u738B\u9CB2+"
        //   objectReference: {fileID: 0}
        var goNameMap = {};
        var lastGoId;
        var crtGoName;
        var quoter = '"';
        var rawLineCache;
        var crossLineCache = null;
        for (var i = 0, len = lines.length; i < len; i++) {
            var oneLine = lines[i];
            var sectionMatch = oneLine.match(this.SectionPattern);
            if (sectionMatch) {
                if (sectionMatch[1] == '1') {
                    lastGoId = sectionMatch[2];
                }
                crtGoName = null;
                continue;
            }
            var nameMatch = oneLine.match(this.NamePattern);
            if (nameMatch && lastGoId) {
                goNameMap[lastGoId] = nameMatch[1];
                lastGoId = null;
            }
            var goMatch = oneLine.match(this.GameObjectPattern);
            if (goMatch) {
                crtGoName = goNameMap[goMatch[1]];
            }
            var quotedContent = '';
            if (null != crossLineCache) {
                rawLineCache += '\n' + oneLine;
                crossLineCache += ' ';
                oneLine = oneLine.replace(/^\s+/, '').replace(/^\\(?=\s)/, '');
                var endRe = new RegExp('(?<!\\\\)' + quoter + '$');
                if (!endRe.test(oneLine)) {
                    // 多行继续
                    crossLineCache += oneLine;
                    continue;
                }
                // 多行结束
                quotedContent = crossLineCache + oneLine.substr(0, oneLine.length - 1);
            }
            else {
                rawLineCache = oneLine;
                var ret = oneLine.match(this.TextPattern);
                if (ret) {
                    quoter = ret[2];
                    var rawContent = ret[3];
                    if (rawContent.charAt(rawContent.length - 1) != quoter) {
                        // 多行待续
                        crossLineCache = rawContent;
                        continue;
                    }
                    quotedContent = rawContent.substr(0, rawContent.length - 1);
                }
            }
            if (quotedContent) {
                quotedContent = this.unicode2utf8(quotedContent);
                for (var _i = 0, keywords_1 = keywords; _i < keywords_1.length; _i++) {
                    var kw = keywords_1[_i];
                    if (quotedContent.indexOf(kw) >= 0) {
                        return { filePath: filePath, goName: crtGoName, matched: quotedContent };
                    }
                }
            }
            crossLineCache = null;
        }
        return null;
    };
    PrefabSearcher.prototype.unicode2utf8 = function (ustr) {
        return ustr.replace(/&#x([\da-f]{1,4});|\\u([\da-f]{1,4})|&#(\d+);|\\([\da-f]{1,4})/gi, function (t, e, n, o, r) { if (o)
            return String.fromCodePoint(o); var c = e || n || r; return /^\d+$/.test(c) && (c = parseInt(c, 10), !isNaN(c) && c < 256) ? unescape("%" + c) : unescape("%u" + c); });
    };
    return PrefabSearcher;
}());
exports.PrefabSearcher = PrefabSearcher;
