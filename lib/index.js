"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var commander_1 = __importDefault(require("commander"));
var fs_1 = __importDefault(require("fs"));
var PrefabSearcher_1 = require("./PrefabSearcher");
var myPackage = require('../package.json');
var rmQuotes = function (val) {
    var rst = val.match(/(['"])(.+)\1/);
    if (rst)
        return rst[2];
    return val;
};
commander_1.default
    .version(myPackage.version, "-v, --version")
    .option("-s, --src <path>", "Source path which contains prefabs to be searched. If not giver, the current work directory will be used.", rmQuotes)
    .option("-k, --keywords [string]", "Specific keywords, seperated with comma.", rmQuotes)
    .parse(process.argv);
var opts = commander_1.default.opts();
console.log("cmd options: " + JSON.stringify(opts));
if (opts.src && !fs_1.default.existsSync(opts.src)) {
    console.error("Source path not exists: " + opts.src);
}
var keywords = opts.keywords.split(',');
if (!keywords.length) {
    console.error("No keywords provided: " + opts.keywords);
}
var ps = new PrefabSearcher_1.PrefabSearcher();
ps.startFind(opts.src || process.cwd(), keywords);
